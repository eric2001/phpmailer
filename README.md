# PHPMailer

## Description
PHPMailer is a plugin for [Gallery 3](http://gallery.menalto.com/) which will allow Gallery to use [PHPMailer](https://github.com/PHPMailer/PHPMailer) instead of php's built in [mail function](http://us2.php.net/manual/en/function.mail.php).

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/89279).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.  

Please note that this module does not include PHPMailer.  you will need to install that on your own by downloading [PHPMailer](https://github.com/PHPMailer/PHPMailer) and extracting the files onto your server somewhere.  This module has been tested with PHPMailer 6.5.1.  Other versions may or may not work.  You'll need the contents of the "src" directory (Exception.php, OAuth.php, PHPMailer.php, POP3.php and SMTP.php) to be in a folder that gallery can access.

Once activated, you will have and Admin -> Settings -> PHPMailer Settings option. Visit this screen to configure the module.  The following options are available.
- **Location of PHPMailer Class:**  The folder location that the actual PHPMailer scripts were copied over to.  Should be a full server path, ex: c:\wamp64\www\phpmailer\src\
- **From Email Address:**  The email address that PHPMailer should use to send emails from.
- **From Name:**  The name associated with the "From" email address.
- **SMTP Server Address:**  The SMTP server that PHPMailer should use to send emails.
- **SMTP Login Name:**  The Login name for the SMTP server.
- **SMTP Password:**  The Password for the SMTP Login name.
- **SMTP Port:**  The Port number for the SMTP Server.
- **Use SSL?:**  Indicates if SSL should be used or not.  If your SMTP server requires a secure connection then check this.
- **Verify Peer?:**  Only used when "Use SSL" is checked.  This should normally be checked unless your server requires special configurations.
- **Verify Peer Name?:**  Only used when "Use SSL" is checked.  This should normally be checked unless your server requires special configurations.
- **Allow Self Signed?:**  Only used when "Use SSL" is unchecked.  This should normally be checked unless your server requires special configurations.

## History
**Version 3.0.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Updated to work with latest PHPMailer release.
> - Added additional configuration options -- Verify Peer, Verify Peer Name and Allow Self Signed.
> - Released 27 August 2021.
>
> Download: [Version 3.0.0](/uploads/a8f33db56230c0d10e41f99f49b513f9/phpmailer300.zip)

**Version 2.1.0:**
> - Tested for use with Gallery 3.0.2.
> - Contains additional error handling and logging to to make troubleshooting any issues easier.
> - Released 01 June 2011.
>
> Download: [Version 2.1.0](/uploads/1fecbef4eab034d358b15e45321e4d49/phpmailer210.zip)

**Version 2.0.0:**
> - Updated for use with Gallery 3.0.1.
> - Added support for the eCard module.
> - Added support for SSL encrypted SMTP servers.
> - Set up default values for config variables.
> - Released 25 April 2011.
>
> Download: [Version 2.0.0](/uploads/de2c552a0bcc1a5c178807104b0f710a/phpmailer200.zip)

**Version 1.4.0:**
> - Updated for Gallery 3.0 Final compatibility.
> - Released 07 October 2010.
>
> Download: [Version 1.4.0](/uploads/380714257d854ceb84c5c9bdc19d0a14/phpmailer140.zip)

**Version 1.3.0:**
> - Updated for Gallery 3 RC-1 compatibility.
> - Released 24 February 2010.
>
> Download: [Version 1.3.0](/uploads/015850c0c09f006d78e756f037f3ec27/phpmailer130.zip)

**Version 1.2.0:**
> - Updated for recent module API changes in Gallery 3.
> - Released 20 January 2010.
>
> Download: [Version 1.2.0](/uploads/6270fdc0be45df774f8abb6b2f339084/phpmailer120.zip)

**Version 1.1.0:**
> - Includes bharat's changes for the recent API update.
> - Released 31 July 2009
>
> Download: [Version 1.1.0](/uploads/7baa029480dc080076b0e9949cf57b71/phpmailer110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 22 July 2009
>
> Download: [Version 1.0.0](/uploads/c0c051183ca95d4212de7ad78abc4436/phpmailer100.zip)
